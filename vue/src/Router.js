import Register from "./components/Register";
import Vue from 'vue';
import VueRouter from 'vue-router';
import Login from "@/components/Login";
import Home from "@/components/Home";
import Cards from "@/components/Cards";
import CardDetails from "@/components/CardDetails";

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    base: __dirname,
    routes: [
        { path: '/', redirect: '/home' },
        {
            path: '/home', component: Home,
            children: [
                { path: 'cards', component: Cards },
                { path: 'carddetails/:id', component: CardDetails, props: true }
            ]
        },
        { path: '/login', component: Login },
        { path: '/register', component: Register }
    ]
})

export default router;
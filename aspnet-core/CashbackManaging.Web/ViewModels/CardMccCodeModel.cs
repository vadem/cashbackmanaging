﻿namespace CashbackManaging.ViewModels
{
    public class CardMccCodeCashbackModel
    {
        public int Id { get; set; }
        
        public MccCodeModel MccCode { get; set; }
        
        public decimal CashbackPercent { get; set; }
    }
}